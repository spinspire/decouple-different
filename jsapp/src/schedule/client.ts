import { buildQuery } from "../lib/graphql";
export async function load(queryid: string, content_type: string, limit: number) {
  const variables = { content_type, limit }
  const query = buildQuery({
    queryid,
    variables,
    transform(result) {
      const { data: { nodeQuery: { entities } } } = result;
      const bytracks = entities
        .map(cur => ({ ...cur, track: cur.fieldTrack.entity.name }))
        .reduce((accum, cur) => {
          accum[cur.track] = accum[cur.track] ? accum[cur.track].concat(cur) : [cur];
          return accum;
        }, {})
      return bytracks;
    }
  });
  return await query.fetch();
}
