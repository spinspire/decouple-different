// get a subset of props from the given src object
export const sub = (src, ...props) =>
  props.reduce((dst, prop) => ({ ...dst, [prop]: src[prop] }), {});
