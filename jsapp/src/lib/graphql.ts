// query shape
//  {
//    query: `...` || queryid: '',
//    variables: { foo: "FOO" },
//    transform(result) {
//      // create and return a new, trasformed version of result
//      return result;
//    }
// }
export function buildQuery(query) {
  return {
    async fetch() {
      let url = "/graphql";
      let options = {};

      // if queryid is set then use GET method
      if (query.queryid) {
        // use GET HTTP method to send the GraphQL query
        url += `?queryid=${query.queryid}`;
        if (query.variables) {
          url += '&variables=' + JSON.stringify(query.variables);
        }
      } else {
        options = {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(query)
        };
      }
      const response = await fetch(url, options);
      const result = await response.json();
      // optionally transform
      return query.transform ? query.transform(result) : result;
    }
  };
}
