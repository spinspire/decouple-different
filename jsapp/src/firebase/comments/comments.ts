import firebase from "firebase/app";
import "firebase/firestore"; // for side-effects
import { readable } from "svelte/store";

export function getCommentsCollection(
  path = null,
  site = null
): firebase.firestore.CollectionReference {
  if (!path) path = window.location.pathname;
  if (!site) site = window.location.hostname;
  path = path.replace(/\//g, "_");
  const collection_path = `event/${site}/paths/${path}/comments`;
  return firebase.firestore().collection(collection_path);
}

export function queryAsStore(query: firebase.firestore.Query) {
  return readable([], (setter) => {
    query.onSnapshot((snapshot) => {
      setter(snapshot.docs.map((doc) => doc.data()));
    });
  });
}
