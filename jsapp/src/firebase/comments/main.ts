import App from './App.svelte';

const selector = ".block-component .content .firebase_comments";
const matches = document.querySelectorAll(selector);
if (matches.length > 0) {
  matches.forEach((target) => {
    new App({
      target,
      props: { ...target.dataset }
    });
  });
} else {
  console.error(`Could not find DOM element matching: '${selector}'. Check the selector and also make sure 'big_pipe' Drupal module is disabled.`);
}
