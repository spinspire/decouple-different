import firebase from "firebase/app";
import "firebase/auth"; // imported only for side-effects
import { readable } from "svelte/store";

// initializes firebase app with the given config
export function init(firebaseConfig) {
  firebase.initializeApp(firebaseConfig);
}

// creates a readonly user store that always reflects the currently logged in user
export function getUserStore() {
  /*
   * create a readonly store with initial value of null
   * and a starter function that listens for auth events
   * and calls this store's setter whenever user auth state changes.
   */
  return readable(null, (setter) => {
    firebase.auth().onAuthStateChanged(setter);
  });
}

// signin using Google auth provider
export function signIn() {
  const provider = new firebase.auth.GoogleAuthProvider();
  firebase.auth().signInWithPopup(provider);
}

export function signOut() {
  firebase.auth().signOut();
}
