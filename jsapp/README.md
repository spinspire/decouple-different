*Note: This project is derived from [SvelteJS Project Template](https://github.com/sveltejs/template). See the README there to learn how this one works.*

---

# SvelteJS "component" apps

This is a single project that can build any number of SvelteJS bundles to be injected into Drupal as "component" blocks. To add a new component, follow the example of the preexisting components. For example, if you want to add a new component `foo` ...

* Create `src/foo/App.svelte` and `src/foo/main.ts` etc.
* Add `'foo'` to `entryPoints` in `rollup.config.js`.
* Add a `foo/foo.component.yml` in `../drupal/web/modules/custom/custom_components/components`
* Add config fields in `foo.component.yml` and corresponding props in `App.svelte`.
* Compile with ...
```
pnpm install
pnpm run querymap
pnpm run build
```
* Clear Drupal cache
* Visit Drupal's `/admin/structure/block` and place the new "component" block in some region on some page and configure it.
* Visit the page, test, debug, code ...
