#next line compiles graphql/*.graphql into output file "q.json"
npx persistgraphql graphql ../drupal/web/modules/custom/custom_components/dist/q.json && \
docker-compose exec php drush php-eval '
    // this PHP code creates a querymap from the graphql queries in "q.json" file
    $json=file_get_contents("modules/custom/custom_components/dist/q.json"); // read "extracted_queries.json" file into $json
    $e=\Drupal\graphql\Entity\QueryMap::create(); // create a QueryMap entity,
    $e->version=sha1($json); // compute SHA1 hash of the file contents and assign to "version"
    $e->map = array_flip((array)json_decode($json)); // decode $json, convert to array, flip array, and assign to "map"
    // try to load querymap in case it alreay exists
    $load = \Drupal\graphql\Entity\QueryMap::load($e->version);
    // finally save the QueryMap entity only if not found
    if(!$load) {
      $e->save();
    }
    echo "Query Hash: {$e->version}\n"; // print the SHA1 to be used for making GraphQL queries
'
