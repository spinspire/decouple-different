*Note: This project is derived from [SpinSpire Drupal Starter Template](https://bitbucket.org/spinspire/drupal-starter-project/). See the README there to learn how this one works.*

---

# Decouple Different
OR
# Islands of App in Ocean of Content

A demo of a different approach to decoupling Drupal. One that does not throw
away the baby with the bath water.

* Serve pages with Drupal, keeping the theming layer and caching layer intact.
* Write front-end apps in JavaScript.
* Embed and configure these apps in Drupal pages as "[component](https://drupal.org/project/component)" blocks.
* Keep the users "anonymous" to Drupal in order to maximize Drupal caching.
* Authenticate users against an external auth system, such as Auth0, Azure AD
  Auth, Firebase Auth, AWS Cognito, OKTA, ForgeRock etc.
* Implement personalization in JS apps using the "external" auth.


# Steps to use it

- `cp .env.example .env` and then edit .env to your taste.
- Edit email addresses and site URL in `drupal/drush/drush.yml`
- Bring up the containers ...

```
docker-compose up -d
```

- Setup your `php` container with ...

```
docker-compose exec --user root php sh /var/www/scripts/init-container.sh
docker-compose exec php composer install
```

- Wait for db container to be ready. Check with ...

```
docker-compose logs -f db
```

- Initialize database and then import configuration from this project ...

```
docker-compose exec php drush site-install
docker-compose exec php drush cim -y
```

- Now visit your website. Either use the public URL (in case you have configured
  one) or use the following to find the Docker IP's URL.

```
echo "http://$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker-compose ps -q web))"
```

- To login, use the user "admin" and the password printed in the step above.
  If you don't remember the password, then generate a temporary login link
  with ...

```
docker-compose exec php drush uli
```
