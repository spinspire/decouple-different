#we might need git during "composer install"
#and mysql-client during "drush sql-drop"
apk add git mysql-client
#install composer
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
#clean up the "root" owned directory and files
rm -rf .composer
