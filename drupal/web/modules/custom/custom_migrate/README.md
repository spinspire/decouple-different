# Create Drupal content from Google Sheets

This module lets you import Drupal content from Google Sheet.
All you have to do is _publicly_ expose a Google Sheet with the right
data organized in rows and columns.

In our case, our data is in [this sheet](https://docs.google.com/spreadsheets/d/15s7CRfpEHlK2RFYnZ5sSWbje_M0uczBp6dBUvhtB7vQ),
which translates to a JSON endpoint of http://spreadsheets.google.com/feeds/list/15s7CRfpEHlK2RFYnZ5sSWbje_M0uczBp6dBUvhtB7vQ/1/public/values?alt=json
The `migrate_plus.migration.node_session.yml` file in
`config/install` maps columns of that Google Sheet into
fields of Drupal's `session` content type and thus creates
nodes of that type upon running the migration.

# Steps to use

* Enable this module (you might have to download dependencies)
* Run `drush migrate:status` to see that a new migration has been configured.
* Run the migration with `drush migrate:import node_session`
